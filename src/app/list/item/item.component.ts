import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../shared/data.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent implements OnInit {
  @Input() itemData: Object; 
  constructor(private dataService: DataService) { }

  ngOnInit() {  
  }

  openModal(){
    this.dataService.sendModal(this.itemData);
  }
}
