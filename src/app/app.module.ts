import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LogoComponent } from './header/logo/logo.component';
import { SearchComponent } from './header/search/search.component';
import { MapComponent } from './map/map.component';
import { FlatpointComponent } from './map/flatpoint/flatpoint.component';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './list/item/item.component';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './shared/data.service';
import { QueryService } from './shared/query.service';
import { ModalComponent } from './shared/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LogoComponent,
    SearchComponent,
    MapComponent,
    FlatpointComponent,
    ListComponent,
    ItemComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [ 
    DataService, 
    QueryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
