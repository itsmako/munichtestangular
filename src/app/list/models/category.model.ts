export class Category{
    id: string;
    title: string;
    href: string;
    type: string;
    system: string;
}