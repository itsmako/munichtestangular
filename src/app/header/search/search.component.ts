import { Component, OnInit } from '@angular/core';
import { QueryService } from '../../shared/query.service';
import { Item } from '../../list/models/item.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass'],
  // providers: [ QueryService ]
})
export class SearchComponent implements OnInit {
  constructor(private query: QueryService) { }

  ngOnInit() {
  }
  onKey(event: any){
    this.query.sendQueryToList(event.target.value);
  }

}
