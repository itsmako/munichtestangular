export interface ModalModel {
    title?: string;
    vicinity?: string;
    category?: string;
    id?: string;
}