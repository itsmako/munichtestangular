import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class QueryService {

    shareDataSubject = new Subject<string>();
    shareData$ = this.shareDataSubject.asObservable(); 
    sendQueryToList( data: string ) {
        if (data) this.shareDataSubject.next(data);
    }

}