import { Component, OnInit } from '@angular/core';
import { Item } from './models/item.model';
import { DataService } from '../shared/data.service';
import { QueryService } from '../shared/query.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass'],
})
export class ListComponent implements OnInit {
  querySubscribtion: Subscription;
  constructor( 
      private dataService: DataService, 
      private queryService: QueryService
      ) {
    
  }
  result: any;
  resultArray: Array<Item>;
  ngOnInit() {
    this.querySubscribtion = this.queryService.shareData$.subscribe(query => {
      this.dataService.getData(query).subscribe(data => {
        this.result = data;
        this.resultArray = this.result.results.splice(1, this.result.results.length);
        this.dataService.passPoints(this.resultArray);
      });
    });
  }

  ngOnDestroy() {
    this.querySubscribtion.unsubscribe();
  }

}
