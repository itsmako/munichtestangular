import { Component, OnInit } from '@angular/core';
import { Item } from '../list/models/item.model';
import { DataService } from '../shared/data.service';

declare let L;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass'],
})
export class MapComponent implements OnInit {
  data: Array<Item>;
  points: [];
  map: any;
  defaultCoordinates: number[];
  constructor(private dataService: DataService) { }

  createMap(center: number[]){
    this.map = L.map('map').setView(center, 13);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  }

  ngOnInit() {
    this.dataService.getPosition().then((result)=>{
        this.createMap(result);
        this.dataService.currentPoints.subscribe(data => {
          if (data) {
            this.map.panTo(data[0]);
            for (let point in data) {
              L.marker(data[point]).addTo(this.map);
            }
          }
        });
      
    });
  }
  

}
