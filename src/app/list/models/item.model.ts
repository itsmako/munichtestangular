import { Category } from "./category.model";
export interface Item {
    title?: string ;
    highlightedTitle?: string;
    vicinity?: string;
    highlightedVicinity?: string;
    position?: number[];
    category?: Category | string; 
    categoryTitle?: string; 
    href?: string;
    type?: string;
    distance?: number; 
    averageRating?: string;
    icon?: string;
    having?: [];
    id?: string;
    chainIds?: [];
}