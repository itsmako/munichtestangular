import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlatpointComponent } from './flatpoint.component';

describe('FlatpointComponent', () => {
  let component: FlatpointComponent;
  let fixture: ComponentFixture<FlatpointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlatpointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlatpointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
