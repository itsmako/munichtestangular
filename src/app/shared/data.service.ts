import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Item } from '../list/models/item.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { element } from '@angular/core/src/render3';

@Injectable()
export class DataService {
    itemUrl = `https://places.cit.api.here.com/places/v1/autosuggest?at=40.74917,-73.98529&app_id=${environment.app_id}&app_code=${environment.app_code}&q=`;
    private pointsSource = new Subject<any>();
    currentPoints = this.pointsSource.asObservable();
    defaultCoordinates = [51.505, -0.09];
    shareModalSubject = new Subject<Object>();
    shareModal$ = this.shareModalSubject.asObservable();
    book = environment.serverUrl;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };
    constructor(private httpClient: HttpClient) {}

    sendModal(data: Object) {
        if (data) this.shareModalSubject.next(data); 
    }

    sendData(query: Object): Observable<Object>{
        console.log('book ', this.book);
        console.log('sended ', query);
        const book = {
            book: {
                city : query['category'],
                id: query['id'],
                property_name: query['title'],
                property_id: query['id'],
                user: 'xyz'
            }
        }
        return this.httpClient.post<Object>(
            this.book + '/properties/book', book, this.httpOptions);
    }

    getData(query) {
        return this.httpClient.get(this.itemUrl+query);
    }

    getPosition(): Promise<any> {
        return new Promise((resolve, reject) => {
            if ('geolocation' in window) {
                navigator.geolocation.getCurrentPosition(resp => {
                    resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
                },
                err => {
                        reject(err);
                });
            }else{
                resolve(this.defaultCoordinates);
            }
        });
    }

    passPoints(points: Array<Item>) {
        points = points.filter( (element) => {
            return element.position != undefined;
        });
        const newPoints = points.map(element => element.position);
        this.pointsSource.next(newPoints);
    }

}