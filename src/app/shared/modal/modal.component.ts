import { Component } from '@angular/core';
import { DataService } from '../../shared/data.service';
import { ModalModel } from '../../list/models/modal.model';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.sass']
})
export class ModalComponent {
  item: ModalModel = {};  
  
  show = false;
  constructor(private dataService: DataService){
    this.dataService.shareModal$.subscribe(res => {
      this.item = res;
      this.show = true;
    });
  }
  book(){
    this.dataService.sendData(this.item).subscribe(res=>{
      console.log('request done')
    }, err=> console.log('Wrong request ',err));
    this.closeModal();
  }
  closeModal(){
    this.show = false;
  }
}
